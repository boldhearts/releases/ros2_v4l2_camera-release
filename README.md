## v4l2_camera (dashing) - 0.1.2-1

The packages in the `v4l2_camera` repository were released into the `dashing` distro by running `/usr/bin/bloom-release v4l2_camera --rosdistro dashing -e` on `Sat, 12 Dec 2020 12:11:14 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: https://gitlab.com/boldhearts/releases/ros2_v4l2_camera-release.git
- rosdistro version: `0.1.1-1`
- old version: `0.1.1-1`
- new version: `0.1.2-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## v4l2_camera (foxy) - 0.3.0-1

The packages in the `v4l2_camera` repository were released into the `foxy` distro by running `/usr/bin/bloom-release v4l2_camera --rosdistro foxy` on `Sat, 26 Sep 2020 12:07:01 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: https://gitlab.com/boldhearts/releases/ros2_v4l2_camera-release.git
- rosdistro version: `0.2.1-3`
- old version: `0.2.1-3`
- new version: `0.3.0-1`

Versions of tools used:

- bloom version: `0.9.8`
- catkin_pkg version: `0.4.22`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.2`
- vcstools version: `0.1.42`


## v4l2_camera (foxy) - 0.2.1-3

The packages in the `v4l2_camera` repository were released into the `foxy` distro by running `/usr/bin/bloom-release v4l2_camera --rosdistro foxy` on `Fri, 07 Aug 2020 11:39:57 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: https://gitlab.com/boldhearts/releases/ros2_v4l2_camera-release.git
- rosdistro version: `0.2.0-1`
- old version: `0.2.1-2`
- new version: `0.2.1-3`

Versions of tools used:

- bloom version: `0.9.7`
- catkin_pkg version: `0.4.22`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.2`
- vcstools version: `0.1.42`


## v4l2_camera (foxy) - 0.2.1-2

The packages in the `v4l2_camera` repository were released into the `foxy` distro by running `/usr/bin/bloom-release v4l2_camera --rosdistro foxy` on `Fri, 07 Aug 2020 11:34:50 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: https://gitlab.com/boldhearts/releases/ros2_v4l2_camera-release.git
- rosdistro version: `0.2.0-1`
- old version: `0.2.1-1`
- new version: `0.2.1-2`

Versions of tools used:

- bloom version: `0.9.7`
- catkin_pkg version: `0.4.22`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.2`
- vcstools version: `0.1.42`


## v4l2_camera (foxy) - 0.2.1-1

The packages in the `v4l2_camera` repository were released into the `foxy` distro by running `/usr/bin/bloom-release v4l2_camera --rosdistro foxy` on `Fri, 07 Aug 2020 11:32:46 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: https://gitlab.com/boldhearts/releases/ros2_v4l2_camera-release.git
- rosdistro version: `0.2.0-1`
- old version: `0.2.0-2`
- new version: `0.2.1-1`

Versions of tools used:

- bloom version: `0.9.7`
- catkin_pkg version: `0.4.22`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.2`
- vcstools version: `0.1.42`


## v4l2_camera (foxy) - 0.2.0-2

The packages in the `v4l2_camera` repository were released into the `foxy` distro by running `/usr/bin/bloom-release v4l2_camera --rosdistro foxy --edit` on `Tue, 23 Jun 2020 19:53:41 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: https://gitlab.com/boldhearts/releases/ros2_v4l2_camera-release.git
- rosdistro version: `0.2.0-1`
- old version: `0.2.0-1`
- new version: `0.2.0-2`

Versions of tools used:

- bloom version: `0.9.7`
- catkin_pkg version: `0.4.20`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.2`
- vcstools version: `0.1.42`


## v4l2_camera (foxy) - 0.2.0-1

The packages in the `v4l2_camera` repository were released into the `foxy` distro by running `/usr/bin/bloom-release v4l2_camera --rosdistro foxy --track foxy --edit` on `Sat, 13 Jun 2020 21:12:52 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: https://gitlab.com/boldhearts/releases/ros2_v4l2_camera-release.git
- rosdistro version: `0.1.1-1`
- old version: `0.1.1-1`
- new version: `0.2.0-1`

Versions of tools used:

- bloom version: `0.9.7`
- catkin_pkg version: `0.4.20`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.2`
- vcstools version: `0.1.42`


## v4l2_camera (foxy) - 0.1.1-1

The packages in the `v4l2_camera` repository were released into the `foxy` distro by running `/usr/bin/bloom-release v4l2_camera --rosdistro foxy` on `Wed, 10 Jun 2020 10:04:15 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.1.1-1`

Versions of tools used:

- bloom version: `0.9.7`
- catkin_pkg version: `0.4.16`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.0`
- vcstools version: `0.1.42`


## v4l2_camera (eloquent) - 0.1.1-1

The packages in the `v4l2_camera` repository were released into the `eloquent` distro by running `/usr/bin/bloom-release v4l2_camera --track eloquent --rosdistro eloquent --new-track` on `Mon, 23 Dec 2019 15:02:03 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.1.1-1`

Versions of tools used:

- bloom version: `0.9.0`
- catkin_pkg version: `0.4.14`
- rosdep version: `0.17.1`
- rosdistro version: `0.7.5`
- vcstools version: `0.1.42`


## v4l2_camera (dashing) - 0.1.1-1

The packages in the `v4l2_camera` repository were released into the `dashing` distro by running `/home/sander/.virtualenvs/ros2/bin/bloom-release --rosdistro dashing --track dashing v4l2_camera` on `Mon, 12 Aug 2019 21:04:01 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: https://gitlab.com/boldhearts/releases/ros2_v4l2_camera-release.git
- rosdistro version: `0.1.0-1`
- old version: `0.1.0-1`
- new version: `0.1.1-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.13`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.42`


## v4l2_camera (dashing) - 0.1.0-1

The packages in the `v4l2_camera` repository were released into the `dashing` distro by running `/home/sander/.virtualenvs/ros2/bin/bloom-release --rosdistro dashing --track dashing v4l2_camera --edit` on `Sun, 11 Aug 2019 19:08:37 -0000`

The `v4l2_camera` package was released.

Version of package(s) in repository `v4l2_camera`:

- upstream repository: https://gitlab.com/boldhearts/ros2_v4l2_camera.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.1.0-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.13`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.42`


# ros2_v4l2_camera-release-release

